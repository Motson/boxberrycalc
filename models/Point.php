<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "point".
 *
 * @property integer $id
 * @property string $code
 * @property string $address
 * @property integer $city_id
 * @property double $geo_lat
 * @property double $geo_long
 * @property integer $is_active
 * @property integer $created_at
 * @property integer $updated_at
 */
class Point extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'point';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'is_active'], 'integer'],
            [['geo_lat', 'geo_long'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['code'], 'string', 'max' => 155],
            [['address'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }
}
