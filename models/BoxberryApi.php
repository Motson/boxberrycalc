<?php
namespace app\models;

class BoxberryApi implements IDeliveryApi
{
    const KEY = '11570.pbpqebfc';
    const URl = 'http://api.boxberry.de/json.php';

    /** Расчет стоимости доставки
     * @param City $toCity
     * @param float $weight Вес в Кг
     * @param int $orderSum Сумма заказа
     * @return float
     */
    public function cost(City $toCity, $weight, $orderSum = 0) {
        $cost = 0;
        if(!empty($toCity->code)) {
            $point = Point::find()->where(['city_id'=>$toCity->id])->one();
            if($point) {
                $inputData = [
                    'target'=>$point->code,
                    'weight'=>$weight*1000, // В граммах
                    'ordersum'=>$orderSum,
                ];
                $resultQuery = $this->_query('DeliveryCosts', $inputData);
                if(!empty($resultQuery) and $resultQuery->error == '') {
                    $cost = $resultQuery->price;
                }
            }
        }
        return $cost;
    }

    /**
     * @return Point[]
     */
    public function getPointList() {
        $points = [];
        $resultQuery = $this->_query('ListPoints');

        if(!empty($resultQuery)) {
            foreach ($resultQuery as $queryItem) {
                $city = City::find()->where(['code'=>$queryItem->CityCode])->one();
                if($city) {
                    $pointModel = new Point();
                    $pointModel->code = $queryItem->Code;
                    $pointModel->address = $queryItem->AddressReduce;
                    $pointModel->city_id = $city->id;
                    $gps = explode(',', $queryItem->GPS);
                    if(!empty($gps[0]) and !empty($gps[1])) {
                        $pointModel->geo_lat = $gps[0];
                        $pointModel->geo_long = $gps[1];
                    }
                    $points[] = $pointModel;
                }
            }
        }
        return $points;
    }

    /**
     * City[] $cities
     */
    public function getCityList() {
        $cities = [];
        $resultQuery = $this->_query('ListCities');
        if(!empty($resultQuery)) {
            foreach ($resultQuery as $queryItem) {
                $cityModel = new City();
                $cityModel->code = $queryItem->Code;
                $cityModel->name = $queryItem->Name;
                $cityModel->country_code = $queryItem->CountryCode;
                $cities[] = $cityModel;
            }
        }
        return $cities;
    }

    protected function _query($functionName, $inputData = []) {
        $inputData['token'] = self::KEY;
        $inputData['method'] = $functionName;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Accept: application/json", "Content-Type: application/json"]);
        $url = self::URl;
        if(!empty($inputData)) {
            $url = $url.'?'.http_build_query($inputData);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $jsonResult = json_decode($result);
        return $jsonResult;
    }
}