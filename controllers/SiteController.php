<?php

namespace app\controllers;

use app\models\BoxberryApi;
use app\models\City;
use app\models\DeliveryForm;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $formModel = new DeliveryForm();
        $cities = City::findAll(['is_active'=>1]);
        $cost = 0;

        if($formModel->load(\Yii::$app->request->post())) {
            if($formModel->validate()) {
                $toCity = City::find()->where(['id'=>$formModel->toCityId])->one();
                if($toCity) {
                    $api = new BoxberryApi();
                    $cost = $api->cost($toCity, $formModel->weight, $formModel->price);
                }
            }
        }


        return $this->render('index', [
            'deliveryForm'=>$formModel,
            'cities'=>$cities,
            'cost'=>$cost
        ]);
    }
}
