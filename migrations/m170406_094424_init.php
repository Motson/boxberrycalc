<?php

use yii\db\Migration;

class m170406_094424_init extends Migration
{
    public function up() {
        $this->createTable('{{city}}', [
            'id'=>$this->primaryKey(),
            'code'=>$this->string(255)->comment('Код города'),
            'country_code'=>$this->string(255)->comment('Код страны'),
            'name'=>$this->string(255)->comment('Имя города'),
            'created_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->comment('Дата обновления'),
            'updated_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')->comment('Дата создания'),
            'is_active'=>$this->integer(11)->defaultValue(1)->comment('Признак активности'),
        ], 'ENGINE InnoDB');
        $this->createIndex('code_index', '{{city}}' ,'code');
        $this->createIndex('country_code_index', '{{city}}' ,'country_code');

        $this->createTable('{{point}}', [
            'id'=>$this->primaryKey(),
            'code'=>$this->string(155),
            'address'=>$this->string(255)->comment('Адрес ПВЗ'),
            'city_id'=>$this->integer(11)->comment('ИД города'),
            'geo_lat'=>$this->float()->comment('Ширина)'),
            'geo_long'=>$this->float()->comment('Долгота)'),
            'is_active'=>$this->integer(11)->defaultValue(1)->comment('Признак активности'),
            'created_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->comment('Дата обновления'),
            'updated_at'=>$this->timestamp()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')->comment('Дата создания'),
        ], 'ENGINE InnoDB');
        $this->createIndex('code_index', '{{point}}', 'code', true);
        $this->createIndex('city_id_index', '{{point}}', 'city_id');
    }

    public function down() {
        $this->dropTable('{{city}}');
        $this->dropTable('{{point}}');
    }
}
