<?php
namespace app\models;

interface IDeliveryApi {

    /**
     * @param City $toCity
     * @param float $weight
     * @param float $orderSum
     * @return float
     */
    public function cost(City $toCity, $weight, $orderSum = 0.0);

    /**
     * @return City[]
     */
    public function getCityList();

    /**
     * @return Point[]
     */
    public function getPointList();
}