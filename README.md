Boxberry Calculator
============================

Простой калькулятор доставки через API Транспортной компании Boxberry.
Стек технологий: Yii2, MySQL5.5+

СТРУКТУРА ДИРЕКТОРИЙ
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources

ТРЕБОВАНИЯ
------------

PHP 5.4+, MySQL5.5+


УСТАНОВКА
------------

Необходимо загрузить все зависимости через Composer

~~~
php composer.phar update
~~~

КОНФИГУРАЦИЯ
-------------

### База данных

Отредактировать файл конфигураии БД `config/db.php` в соответствии с Вашей конфигурацией:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=boxberrycalc',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];
```

Запустите миграцию для создания структуры базы данных
```php
php yii migrate
```

### Предвариельная загрузка данных
Некоторые данные должны быть предварительно загружены (города, пункты выдачи заказов). Для этого выполните комманду:
```php
php yii delivery
```