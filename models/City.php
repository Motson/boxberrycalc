<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $country_code
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_active
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
//                'attributes' => [
//                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
//                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
//                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['is_active'], 'integer'],
            [['name', 'code', 'country_code'], 'string', 'max' => 255],
        ];
    }
}
