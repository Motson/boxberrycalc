<?php
/**
 * @var $this yii\web\View
 * @var \app\models\DeliveryForm $deliveryForm
 * @var \app\models\City[] $cities
 * @var float $cost
 */
use \yii\bootstrap\ActiveForm;
use \yii\helpers\ArrayHelper;
?>
<div class="site-index">

    <div class="row">
        <div class="col-lg-12   ">
            <h1>Калькулятор доставки</h1>
        </div>
    </div>
    <hr class="push10">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <?
                $form = ActiveForm::begin([
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-2',
                            'wrapper' => 'col-sm-4',
                        ],
                    ],
                ]);
                ?>


                <?= $form->field($deliveryForm, 'toCityId')->dropDownList(
                        ArrayHelper::map($cities, 'id', 'name')
                ) ?>

                <?= $form->field($deliveryForm, 'weight')->textInput([
                    'type' => 'number',
                ]); ?>

                <?= $form->field($deliveryForm, 'price')->textInput([
                    'type' => 'number',
                ]); ?>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-4">
                        <button type="submit" class="btn btn-primary">Рассчитать</button>
                    </div>
                </div>

                <? ActiveForm::end(); ?>

                <? if($cost): ?>
                    <div class="col-sm-6">
                        <div class="alert alert-success">
                            Стоимость доставки: <?=$cost?> р.
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>

    </div>
</div>
