<?php
namespace app\commands;
use app\models\BoxberryApi;
use app\models\City;
use app\models\IDeliveryApi;
use app\models\Point;
use yii\console\Controller;

class DeliveryController extends Controller
{

    public function actionIndex() {
        $this->actionUpdateCities();
        $this->actionUpdatePoints();
    }

    public function actionUpdateCities() {
        $this->updateCities(new BoxberryApi());
    }

    public function actionUpdatePoints() {
        $this->updatePoints(new BoxberryApi());
    }

    /**
     * @param IDeliveryApi $api
     */
    public function updateCities(IDeliveryApi $api) {
        $activeIds = [];
        $cities = $api->getCityList();
        foreach ($cities as $cityModel) {
            $currentCity = City::find()->where(['code'=>$cityModel->code])->one();
            if(empty($currentCity)) {
                $currentCity = new City();
                $currentCity->code = $cityModel->code;
            }
            $currentCity->setAttributes([
                'name'=>$cityModel->name,
                'country_code'=>$cityModel->country_code,
                'is_active'=>1,
            ]);
            if($currentCity->save()) {
                echo "City updated. Id: {$currentCity->id}\n";
                $activeIds[] = $currentCity->id;
            }
        }
        if(!empty($activeIds)) {
            City::updateAll(['is_active'=>0], ['not in', 'id', $activeIds, 'is_active'=>1]);
        }
    }


    /**
     * @param IDeliveryApi $api
     */
    public function updatePoints(IDeliveryApi $api) {
        $activeIds = [];
        $points = $api->getPointList();
        foreach ($points as $pointModel) {
            $currentPoint = Point::find()->where(['code'=>$pointModel->code])->one();
            if(empty($currentPoint)) {
                $currentPoint = new Point();
                $currentPoint->code = $pointModel->code;
            }
            $currentPoint->setAttributes([
                'address'=>$pointModel->address,
                'city_id'=>$pointModel->city_id,
                'geo_lat'=>$pointModel->geo_lat,
                'geo_long'=>$pointModel->geo_long,
                'is_active'=>1,
            ]);
            if($currentPoint->save()) {
                echo "Point updated. Id: {$currentPoint->id}\n";
                $activeIds[] = $currentPoint->id;
            }
        }
        if(!empty($activeIds)) {
            Point::updateAll(['is_active'=>0], ['not in', 'id', $activeIds, 'is_active'=>1]);
        }
    }
}
