<?php

namespace app\models;

use yii\base\Model;

/**
 * DeliveryForm is the model behind the api calculator form.
 */
class DeliveryForm extends Model
{
    const MAX_WEIGHT = 31;
    const MAX_PRICE = 200000;
    
    public $toCityId;
    public $weight;
    public $price;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['toCityId', 'weight', 'price'], 'required'],
            ['weight', 'double', 'min' => 1, 'max'=>self::MAX_WEIGHT],
            ['price', 'double', 'min' => 1, 'max'=>self::MAX_PRICE],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'toCityId' => 'Город назначения',
            'weight' => 'Вес товара, Кг',
            'price' => 'Стоимость товара',
        ];
    }
}
